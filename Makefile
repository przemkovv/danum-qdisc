obj-m += danum-qdisc.o
#obj-m += danum-dispatch.o
obj-m += danum-signalling.o
danum-qdisc-objs := qdisc.o
#danum-dispatch-objs := dispatcher.o
danum-signalling-objs := signalling.o
#nfdebug.o 

#HOSTCC = gcc -m32
#EXTRA_CFLAGS = -m32
#TARGET_ARCH = x86
M ?= $(shell pwd)

KVERSION ?= $(shell uname -r)

all: 
	echo $(M)
	make -C /lib/modules/$(KVERSION)/build M=$(M) modules

clean:
	make -C /lib/modules/$(KVERSION)/build M=$(M) clean
	rm -rf docs/html
	rm -rf docs/latex
	rm -f ./*.pyc

doc:
	doxygen

clean-tags:
	rm -rf cscope.*
	rm -rf tags

tags:
	sh ../src/tools/tags.sh cscope
	sh ../src/tools/tags.sh tags    	


doomish:
	cp *.ko ~/virtual_machines/doomish/
