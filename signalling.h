#ifndef DANUM_SIGNALLING_H
#define DANUM_SIGNALLING_H

#include <net/flow_keys.h>

struct signalling_Qdisc_ops {
    int dummy;
};


static void danum_signalling_register_qdisc(struct Qdisc *sch, struct signalling_Qdisc_ops *ops) ;
static void danum_signalling_unregister_qdisc(struct Qdisc *sch, struct signalling_Qdisc_ops *ops);

static void danum_signalling_start_monitor_flow(struct Qdisc *sch, struct flow_keys *flow_keys, unsigned int hash);
static void danum_signalling_stop_monitor_flow(unsigned int hash);


#endif
