#!/bin/bash

set -x

tc qdisc del dev eth1 root

rmmod danum_qdisc
rmmod danum_signalling
insmod ../danum-signalling.ko
insmod ../danum-qdisc.ko
tc qdisc add dev eth1 root handle 1: danum

