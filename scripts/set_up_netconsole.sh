#!/bin/bash

set -x
dmesg -n 8

modprobe netconsole
if [[ $? != 0 ]]; then exit $?; fi

mkdir /sys/kernel/config/netconsole/gandalf
cd /sys/kernel/config/netconsole/gandalf

echo 0 > enabled

echo eth2 > dev_name
echo 192.168.34.11 > local_ip
echo 192.168.34.1 > remote_ip
arping `cat remote_ip` -f -I eth2 | grep -o ..:..:..:..:..:.. > remote_mac

echo 1 > enabled
