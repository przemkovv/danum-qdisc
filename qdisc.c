#include <linux/module.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/list.h>
#include <linux/spinlock_types.h>
#include <linux/workqueue.h>
#include <net/pkt_sched.h>
#include <net/sch_generic.h>
#include <net/flow_keys.h>
#include <linux/jhash.h>

#include "common.h"
#include "qdisc.h"
#include "signalling.h"

#define MODULE_NAME	"QDISC"

#define DANUM_DEBUG_PACKET_ENQUEUE  1
#define DANUM_DEBUG_PACKET_DEQUEUE  1

#define DANUM_DEFAULT_HASH_DIVISOR  1024
#define DANUM_EMPTY_SLOT	    0x0
#define DANUM_CLEANUP_THRESHOLD	    10
#define DANUM_CLEANUP_WORK_DELAY    1 * HZ


typedef u16 danum_index;

DEFINE_SPINLOCK(danum_slots_list_lock);

struct workqueue_struct *danum_workqueue;
void danum_slot_cleanup_work(struct work_struct *data);

struct danum_work{
    struct delayed_work delayed_work;
    struct Qdisc *sch;
};

struct danum_work* danum_work;

struct danum_slot {
	struct list_head list;
	struct rcu_head rcu;

	struct Qdisc *q; // own implementation of queue or use Qdisc?
	unsigned int is_empty;

	unsigned short	hash; /* hash value (index in ht[]) */

	struct flow_keys flow_keys;
};

struct danum_sched_data {
    unsigned int	divisor;	    /* number of slots in hash table */

    u32			perturbation;


    struct list_head	slots_list;
    danum_index		slots_len;
    struct danum_slot **ht;

    unsigned int qlen;
};

struct danum_skb_cb {
    struct flow_keys        keys;
};


static inline struct danum_skb_cb *danum_skb_cb(const struct sk_buff *skb)
{
    qdisc_cb_private_validate(skb, sizeof(struct danum_skb_cb));
    return (struct danum_skb_cb *)qdisc_skb_cb(skb)->data;
}

static unsigned int danum_hash(const struct danum_sched_data *q,
	const struct sk_buff *skb)
{
    const struct flow_keys *keys = &danum_skb_cb(skb)->keys;
    unsigned int hash;

    hash = jhash_3words((__force u32)keys->dst,
	    (__force u32)keys->src ^ keys->ip_proto,
	    (__force u32)keys->ports, q->perturbation);
    return hash & (q->divisor - 1);
}

static unsigned int danum_classify(struct sk_buff *skb, struct Qdisc *sch)
{
    struct danum_sched_data *priv = qdisc_priv(sch);
    
    skb_flow_dissect(skb, &danum_skb_cb(skb)->keys);
    return danum_hash(priv, skb) + 1;
}


static void dump_flow_keys(struct flow_keys *keys) {
    /* struct flow_keys:
     *	@src: source ip address in case of IPv4
     *	      For IPv6 it contains 32bit hash of src address
     *	@dst: destination ip address in case of IPv4
     *	      For IPv6 it contains 32bit hash of dst address
     *	@ports: port numbers of Transport header
     *		port16[0]: src port number
     *		port16[1]: dst port number
     *	@thoff: Transport header offset
     *	@n_proto: Network header protocol (eg. IPv4/IPv6)
     *	@ip_proto: Transport header protocol (eg. TCP/UDP)
     * All the members, except thoff, are in network byte order.
     */
    printk("%pI4:%d - %pI4:%d, P: %d", &keys->src, keys->port16[0],
				       &keys->dst, keys->port16[1],
				       keys->ip_proto);
}

static struct danum_slot *danum_alloc_slot(struct Qdisc *sch, struct flow_keys *flow_keys, unsigned int hash) {
    struct danum_slot *slot;
    struct danum_sched_data *priv;

    priv = qdisc_priv(sch);
    printk(DANUM_INFO "Allocating slot %x\n", hash);

    slot = danum_alloc(sizeof(*slot));
    slot->q = qdisc_create_dflt(sch->dev_queue, &pfifo_qdisc_ops, hash);
    if (!slot->q) 
	printk(DANUM_INFO "Something went wrong with creating default queue\n");

    slot->hash = hash;
    slot->flow_keys = *flow_keys;
    return slot;
}

static void danum_free_slot(struct danum_slot *slot) {
    qdisc_destroy(slot->q);
    danum_free(slot);
}

static void danum_free_slot_rcu(struct rcu_head *rhp) {
    danum_free_slot(container_of(rhp, struct danum_slot, rcu));
}

static int danum_enqueue(struct sk_buff *skb, struct Qdisc *sch)
{

    int ret;
    unsigned int hash;
    struct danum_slot *slot = NULL;
    struct danum_sched_data *priv;

    hash = danum_classify(skb, sch);
#ifdef DANUM_DEBUG_PACKET_ENQUEUE
    printk(DANUM_INFO "Enqueing packet, keys: ");
    dump_flow_keys(&danum_skb_cb(skb)->keys);
    printk(" hash: %x ", hash);
#endif

    priv = qdisc_priv(sch);
    slot = priv->ht[hash];
    
    if (slot == DANUM_EMPTY_SLOT) {
#ifdef DANUM_DEBUG_PACKET_ENQUEUE
	printk(DANUM_INFO "slot %d/%x is empty\n", hash, hash);
#endif
	slot = danum_alloc_slot(sch, &danum_skb_cb(skb)->keys, hash);
#ifdef DANUM_DEBUG_PACKET_ENQUEUE
	printk(DANUM_INFO "new slot %p", slot);
#endif
	spin_lock(&danum_slots_list_lock);
	list_add_rcu(&slot->list, &priv->slots_list);
	priv->slots_len++;
	spin_unlock(&danum_slots_list_lock);
	priv->ht[hash] = slot;

	danum_signalling_start_monitor_flow(sch, &slot->flow_keys, slot->hash);
    }

    if (!slot->q) 
	printk(DANUM_INFO "Something went wrong with slot queue\n");

    /*if (likely(skb_queue_len(&slot->q) < sch->limit))*/
    ret =  qdisc_enqueue_tail(skb, slot->q);
    sch->q.qlen++;
#ifdef DANUM_DEBUG_PACKET_ENQUEUE
    printk("slot_qlen %d, global_qlen %d\n", qdisc_qlen(slot->q), sch->q.qlen);
#endif
    slot->is_empty = 0;

    return ret;
}

static struct sk_buff *danum_dequeue(struct Qdisc *sch)
{
    struct danum_sched_data *priv;
    struct danum_slot* slot;
    struct sk_buff *skb = NULL;
    priv = qdisc_priv(sch);

    if (sch->gso_skb) 
	printk(DANUM_INFO "There is something in gso_skb. I wonder...");

    rcu_read_lock();
    list_for_each_entry_rcu(slot, &priv->slots_list, list) {
	if (qdisc_qlen(slot->q)) {
	    skb = qdisc_dequeue_head(slot->q);
	    sch->q.qlen--;
#ifdef DANUM_DEBUG_PACKET_DEQUEUE
	    printk(DANUM_INFO "Dequeing packet. Queue %d/%x len = %d, global_qlen = %d\n", slot->hash,
		    slot->hash, qdisc_qlen(slot->q), sch->q.qlen);
#endif
	    break;
	} else {
	    slot->is_empty++;
#ifdef DANUM_DEBUG_PACKET_DEQUEUE
	    if (slot->is_empty > DANUM_CLEANUP_THRESHOLD) {
		printk(DANUM_INFO "Queue %d/%x is ready to cleanup (%d)\n", slot->hash, slot->hash, slot->is_empty);
	    }
#endif
	}

    }
    rcu_read_unlock();
    if (skb) {
	spin_lock(&danum_slots_list_lock);
	list_move_tail(&slot->list, &priv->slots_list);
	spin_unlock(&danum_slots_list_lock);
    }

#ifdef DANUM_DEBUG_PACKET_DEQUEUE
    printk(DANUM_INFO "Dequeue: Qdisc global qlen %d\n", sch->q.qlen);
#endif
    return skb;
}

void danum_slot_cleanup_work(struct work_struct *data) 
{
    struct danum_sched_data *priv;
    struct danum_slot *slot, *tmp;
    priv = qdisc_priv(((struct danum_work*) data)->sch);

    list_for_each_entry_safe(slot, tmp, &priv->slots_list, list) {
	if (slot->is_empty > DANUM_CLEANUP_THRESHOLD) {
	    priv->ht[slot->hash] = DANUM_EMPTY_SLOT;
	    danum_signalling_stop_monitor_flow(slot->hash);

	    spin_lock(&danum_slots_list_lock);
	    list_del_rcu(&slot->list);
	    priv->slots_len--;
	    spin_unlock(&danum_slots_list_lock);
	    call_rcu(&slot->rcu, danum_free_slot_rcu);
	}
    }
    queue_delayed_work(danum_workqueue, (struct delayed_work*)data, DANUM_CLEANUP_WORK_DELAY);
}

static struct signalling_Qdisc_ops  danum_signalling_qdisc_ops __read_mostly = {
	.dummy		=	0,
};
EXPORT_SYMBOL(danum_signalling_qdisc_ops);

/* Destroys the resources used during initialization of the queueing discipline */
static void danum_destroy(struct Qdisc *sch)
{
    struct danum_sched_data *priv;
    struct danum_slot *slot, *tmp;
    priv =  qdisc_priv(sch);

    cancel_delayed_work((struct delayed_work *) danum_work);
    danum_signalling_unregister_qdisc(sch, &danum_signalling_qdisc_ops);

    danum_free(priv->ht);

    spin_lock(&danum_slots_list_lock);
    list_for_each_entry_safe(slot, tmp, &priv->slots_list, list) {
	list_del_rcu(&slot->list);
	priv->slots_len--;
	danum_free_slot(slot);
    }
    spin_unlock(&danum_slots_list_lock);
}

/* Initialize new queueing discipline */
static int danum_init(struct Qdisc *sch, struct nlattr *opt)
{
    u32 limit;
    struct danum_sched_data *priv;
    int i;

    printk(DANUM_INFO "Initializing module");
    limit = qdisc_dev(sch)->tx_queue_len ? qdisc_dev(sch)->tx_queue_len : 1;
    limit *= psched_mtu(qdisc_dev(sch));
    sch->limit = limit;

    priv =  qdisc_priv(sch);
    priv->perturbation = 0;
    priv->divisor = DANUM_DEFAULT_HASH_DIVISOR;


    INIT_LIST_HEAD(&priv->slots_list);
    priv->slots_len = 0;

    sch->q.qlen = 0;

    priv->ht = danum_alloc(sizeof(priv->ht[0]) * priv->divisor);
    // initialize queues
    if (!priv->ht) {
	danum_destroy(sch);
	return -ENOMEM;
    }
    for (i = 0; i < priv->divisor; i++)
	priv->ht[i] = DANUM_EMPTY_SLOT;

    if (danum_workqueue) {
	danum_work = (struct danum_work*) kmalloc(sizeof(struct danum_work), GFP_KERNEL);
	danum_work->sch = sch;
	if (danum_work) {
	    INIT_DELAYED_WORK((struct delayed_work*) danum_work, danum_slot_cleanup_work);
	    queue_delayed_work(danum_workqueue, (struct delayed_work *) danum_work, DANUM_CLEANUP_WORK_DELAY);
	}
    }
    danum_signalling_register_qdisc(sch, &danum_signalling_qdisc_ops);

    return 0;
}

/* Shows the statistics of the queueing discipline. */
static int danum_dump(struct Qdisc *sch, struct sk_buff *skb)
{
    return 0;
}
static struct sk_buff *danum_peek(struct Qdisc *sch)
{
    return qdisc_peek_head(sch);
}

/* Resets the queueing discipline back to the initial state. */
static void danum_reset(struct Qdisc *sch)
{
    qdisc_reset_queue(sch);
}


/* Removes the packet from the queue and drops it. */
static unsigned int danum_drop(struct Qdisc *sch)
{
    return qdisc_queue_drop(sch);
}

/* Changes values of the parameters of a queueing discipline */
/*static int danum_tune(struct Qdisc *sch, struct nlattr *opt)*/
/*{*/
/*}*/

static struct Qdisc_ops danum_qdisc_ops __read_mostly = {
	.next		=	NULL,
	.id		=	"danum",
	.priv_size	=	sizeof(struct danum_sched_data),
	.enqueue	=	danum_enqueue,
	.dequeue	=	danum_dequeue,
	.peek		=	qdisc_peek_dequeued,
	.drop		=	danum_drop,
	.init		=	danum_init,
	.reset		=	danum_reset,
	.destroy	=	danum_destroy,
	/*.change		=	danum_tune,*/
	.dump		=	danum_dump,
	.owner		=	THIS_MODULE,
};
EXPORT_SYMBOL(danum_qdisc_ops);

static int __init danum_qdisc_module_init(void)
{
    printk(DANUM_INFO "Hello world from DANUM qdisc module\n");

    danum_workqueue = create_singlethread_workqueue("danum_slot_cleanup_work");

    return register_qdisc(&danum_qdisc_ops);
}

static void __exit danum_qdisc_module_exit(void)
{
    flush_workqueue(danum_workqueue);
    destroy_workqueue(danum_workqueue);

    unregister_qdisc(&danum_qdisc_ops);


    printk(DANUM_INFO "Bye from DANUM qdisc module!\n");
}

module_init(danum_qdisc_module_init)
module_exit(danum_qdisc_module_exit)

MODULE_VERSION("0.1");
MODULE_LICENSE("GPL");

MODULE_AUTHOR("Przemysław Walkowiak");
MODULE_DESCRIPTION("DANUM qdisc module");
