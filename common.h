#ifndef DANUM_COMMON_H
#define DANUM_COMMON_H

#define DANUM_INFO	KERN_INFO "DANUM " MODULE_NAME ": "
#define DANUM_DEBUG	KERN_DEBUG "DANUM " MODULE_NAME ": "

static void *danum_alloc(size_t sz)
{
	void *ptr = kmalloc(sz, GFP_KERNEL | __GFP_NOWARN);

	if (!ptr)
		ptr = vmalloc(sz);
	return ptr;
}

static void danum_free(void *addr)
{
	kvfree(addr);
}
#endif
