#include <linux/module.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/list.h>
#include <linux/spinlock_types.h>
#include <linux/workqueue.h>
#include <net/pkt_sched.h>
#include <net/sch_generic.h>
#include <net/flow_keys.h>
#include <linux/jhash.h>

#include "common.h"
#include "signalling.h"

DEFINE_SPINLOCK(danum_signalling_qdisc_lock);

#define MODULE_NAME	"SIGNALLING"


struct signalling_Qdisc {
	struct Qdisc *q;
	struct signalling_Qdisc_ops *ops;

	struct list_head qdisc_list;
	struct rcu_head rcu;

};

/*struct list_head danum_qdisc_list;*/
LIST_HEAD(danum_qdisc_list);


static void danum_free_signalling_qdisc(struct signalling_Qdisc *q) {
    danum_free(q);
}

static void danum_free_signalling_qdisc_rcu(struct rcu_head *rhp) {
    danum_free_signalling_qdisc(container_of(rhp, struct signalling_Qdisc, rcu));
}

static void danum_signalling_register_qdisc(struct Qdisc *sch, struct signalling_Qdisc_ops *ops) {
    
	struct signalling_Qdisc* signalling_qdisc;
	signalling_qdisc = danum_alloc(sizeof(*signalling_qdisc));
	signalling_qdisc->q = sch;
	signalling_qdisc->ops = ops;

	spin_lock(&danum_signalling_qdisc_lock);
	list_add_rcu(&signalling_qdisc->qdisc_list, &danum_qdisc_list);
	spin_unlock(&danum_signalling_qdisc_lock);
	printk(DANUM_INFO "registered queue %p/%p\n", sch, ops);

}
EXPORT_SYMBOL(danum_signalling_register_qdisc);

static void danum_signalling_unregister_qdisc(struct Qdisc *sch, struct signalling_Qdisc_ops *ops) {

    struct signalling_Qdisc* signalling_qdisc, *tmp;
    list_for_each_entry_safe(signalling_qdisc, tmp, &danum_qdisc_list, qdisc_list) {
	if (signalling_qdisc->q == sch){
	    printk(DANUM_INFO "unregistering qdisc\n");
	    spin_lock(&danum_signalling_qdisc_lock);
	    list_del_rcu(&signalling_qdisc->qdisc_list);
	    spin_unlock(&danum_signalling_qdisc_lock);
	    call_rcu(&signalling_qdisc->rcu, danum_free_signalling_qdisc_rcu);
	    break;
	}
    }
    synchronize_rcu();

    printk(DANUM_INFO "unregistered queue %p/%p\n", sch, ops);
}
EXPORT_SYMBOL(danum_signalling_unregister_qdisc);

static void danum_signalling_start_monitor_flow(struct Qdisc *sch, struct flow_keys *flow_keys, unsigned int hash){

}
EXPORT_SYMBOL(danum_signalling_start_monitor_flow);

static void danum_signalling_stop_monitor_flow(unsigned int hash){

}
EXPORT_SYMBOL(danum_signalling_stop_monitor_flow);



static int __init danum_signalling_module_init(void)
{
    printk(DANUM_INFO "Hello world from DANUM signalling module\n");


    return 0;
}

static void __exit danum_signalling_module_exit(void)
{

    if (list_empty(&danum_qdisc_list)) {
	printk(DANUM_INFO "Some qdisc did not unregister from signalling\n");
    }

    printk(DANUM_INFO "Bye from DANUM signalling module!\n");
}

module_init(danum_signalling_module_init)
module_exit(danum_signalling_module_exit)

MODULE_VERSION("0.1");
MODULE_LICENSE("GPL");

MODULE_AUTHOR("Przemysław Walkowiak");
MODULE_DESCRIPTION("DANUM signalling module");
